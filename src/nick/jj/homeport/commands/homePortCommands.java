package nick.jj.homeport.commands;

import nick.jj.homeport.base.chatMessageFormater;
import nick.jj.homeport.base.platformChecker;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;


public class homePortCommands implements CommandExecutor {
    Plugin plugin;

    public homePortCommands(Plugin pluginInstance) {
        plugin = pluginInstance;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String [] args) {
        if (!(sender instanceof Player)) {return true;};

        if (cmd.getName().equalsIgnoreCase("homeport")) {
            World world = ((Player) sender).getWorld();
            Material mediumBlock = Material.BOOK;

            if (args.length == 0) {
                String help = (ChatColor.BOLD + "Easy teleporting platform.\n") + ChatColor.RESET +
                        (ChatColor.ITALIC + "" +ChatColor.YELLOW + "/homeport settarget ") + (ChatColor.RESET + "Write actual position into book as target position.") + ChatColor.RESET + "\n"+
                        (ChatColor.ITALIC + "" +ChatColor.YELLOW + "/homeport settarget emptyOnly ") + (ChatColor.RESET + "Players will be teleported only with empty inventory.") + ChatColor.RESET + "\n";
                chatMessageFormater.sendTo(sender, help);
            }


            if (args.length > 0) {
                Player player = ((Player) sender);
                PlayerInventory inventory = player.getInventory();
                if (inventory.getItemInMainHand().getType() != mediumBlock) {
                    chatMessageFormater.sendTo(sender, "You need to hold a book to create target record.");
                    return false;
                }
                // (inventory.getItemInMainHand().getAmount() > 1)

                // chatMessageFormater.sendTo(sender,"Argument found:" + args[0]);
                if (args[0].equalsIgnoreCase("settarget")) {
                    if (!platformChecker.chechPlatform(player.getLocation(), true)) {
                        chatMessageFormater.sendTo(sender,"There is no target platform.");
                        return false;
                    }
                    byte flagsByte = 0b00000000;
                    String flagString = "";
                    if (args.length > 1) {
                        if (args[1].equalsIgnoreCase("emptyOnly")) {flagsByte |= ((byte) 0b00000001);}
                        flagString += "emptyOnly, ";
                    }
                    ItemStack stack = new ItemStack(mediumBlock, 1);
                    ItemMeta meta = stack.getItemMeta();
                    meta.setDisplayName(ChatColor.YELLOW + "Homeport Target");

                    Integer playerX = player.getLocation().getBlockX();
                    Integer playerY = player.getLocation().getBlockY();
                    Integer playerZ = player.getLocation().getBlockZ();

                    meta.getPersistentDataContainer().set(new NamespacedKey(plugin, "targetX"), PersistentDataType.INTEGER, playerX);
                    meta.getPersistentDataContainer().set(new NamespacedKey(plugin, "targetY"), PersistentDataType.INTEGER, playerY);
                    meta.getPersistentDataContainer().set(new NamespacedKey(plugin, "targetZ"), PersistentDataType.INTEGER, playerZ);
                    meta.getPersistentDataContainer().set(new NamespacedKey(plugin, "targetWorld"), PersistentDataType.STRING, player.getWorld().getName());
                    meta.getPersistentDataContainer().set(new NamespacedKey(plugin, "flags"), PersistentDataType.BYTE, flagsByte);
                    List<String> lore = new ArrayList<>();
                    lore.add(ChatColor.GOLD + "Book holding target");
                    lore.add(ChatColor.GOLD + "location for Homeport.");
                    meta.setLore(lore);
                    meta.addEnchant(Enchantment.SOUL_SPEED, 1, true);
                    meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    stack.setItemMeta(meta);
                    inventory.getItemInMainHand().setAmount(inventory.getItemInMainHand().getAmount() - 1);
                    inventory.addItem(stack);
                    if (flagString == "") {
                        chatMessageFormater.sendTo(sender, "Homeport target created! Nice.");
                    } else {
                        chatMessageFormater.sendTo(sender, "Homeport target created with flags: " + flagString);
                    }
                }

                if (args[0].equalsIgnoreCase("strikeme")) {
                    chatMessageFormater.sendTo(sender,"Strinking!");
                    // world.strikeLightning(((Player) sender).getLocation());
                    world.spawnParticle(Particle.ELECTRIC_SPARK, ((Player) sender).getLocation(), 5000, 0.5, 2, 0.5);
                }
            }

        }
        return true;
    }
}
