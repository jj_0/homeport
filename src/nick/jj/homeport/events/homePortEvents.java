package nick.jj.homeport.events;

import nick.jj.homeport.base.chatMessageFormater;
import nick.jj.homeport.base.platformChecker;
import nick.jj.homeport.base.playerGetter;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.Switch;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.block.NotePlayEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

// import static org.bukkit.Bukkit.getServer;


public class homePortEvents implements Listener {
    Plugin plugin;
    public homePortEvents(Plugin plugin) {
        this.plugin = plugin;
    }

    /*
    @EventHandler
    public static void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        player.sendMessage(ChatColor.BOLD + "Fuck you!");
        // getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "[homePort running.]");
    }
    */


    @EventHandler
    public void onBlockRedstone(BlockRedstoneEvent event) {
        Block block = event.getBlock();
        if (block.getType() == Material.STONE_BUTTON) {
            // BlockData data = block.getBlockData();
            Switch data = (Switch) block.getBlockData();
            if (!data.isPowered()) {
                // Bukkit.broadcastMessage(data.getFacing().toString());
                Location location = block.getLocation();
                World world = block.getWorld();
                int centerX = location.getBlockX();
                int centerY = location.getBlockY();
                int centerZ = location.getBlockZ();
                if (data.getFacing() == BlockFace.SOUTH) {centerZ -= 1;}
                if (data.getFacing() == BlockFace.NORTH) {centerZ += 1;}
                if (data.getFacing() == BlockFace.WEST)  {centerX += 1;}
                if (data.getFacing() == BlockFace.EAST)  {centerX -= 1;}
                Block centerBlock = world.getBlockAt(centerX, centerY, centerZ);
                if ((centerBlock.getType() == Material.GOLD_BLOCK) || (centerBlock.getType() == Material.DIAMOND_BLOCK)) {
                    // Bukkit.broadcastMessage("Gold block OK");
                    ItemFrame frame = null;

                    for(ItemFrame checkedFrame : world.getEntitiesByClass(ItemFrame.class)) {
                        Location frameLocation = checkedFrame.getLocation();
                        // Bukkit.broadcastMessage(frameLocation.toString());
                        if (
                                (frameLocation.getBlockX() == centerX) &&
                                        (frameLocation.getBlockY() == centerY + 1) &&
                                        (frameLocation.getBlockZ() == centerZ)
                        ) {
                            frame = checkedFrame;
                        }
                    }

                    if (frame == null) {return;}

                    // Bukkit.broadcastMessage("Frame found!");
                    ItemStack stack = frame.getItem();
                    ItemMeta meta = stack.getItemMeta();
                    Location centerLocation = new Location(world, centerX, centerY, centerZ);
                    ArrayList<Player> portablePlayer = playerGetter.nearLocation(centerLocation, 2, 1);

                    if (!platformChecker.chechPlatform(centerLocation, false)) {
                        for (Player player : portablePlayer) {
                            chatMessageFormater.sendTo(player, "This Homeport platform is not working.");
                        }
                        return;
                    }

                    PersistentDataContainer container = meta.getPersistentDataContainer();
                    if (container == null) {
                        return;
                    }

                    if (!container.has(new NamespacedKey(plugin, "targetX"),  PersistentDataType.INTEGER)) {
                        return;
                    }
                    Integer targetX = meta.getPersistentDataContainer().get(new NamespacedKey(plugin, "targetX"),  PersistentDataType.INTEGER);
                    Integer targetY = meta.getPersistentDataContainer().get(new NamespacedKey(plugin, "targetY"),  PersistentDataType.INTEGER);
                    Integer targetZ = meta.getPersistentDataContainer().get(new NamespacedKey(plugin, "targetZ"),  PersistentDataType.INTEGER);
                    String targetWorldName = meta.getPersistentDataContainer().get(new NamespacedKey(plugin, "targetWorld"), PersistentDataType.STRING);
                    byte flagsByte = meta.getPersistentDataContainer().get(new NamespacedKey(plugin, "flags"), PersistentDataType.BYTE);
                    ////////////////////////////////////////////////////////////////////////////////////////////////

                    World targetWorld = world;
                    if (!targetWorldName.equalsIgnoreCase(centerBlock.getWorld().getName())) {
                        if (centerBlock.getType() == Material.DIAMOND_BLOCK) {
                            targetWorld = Bukkit.getWorld(targetWorldName);
                        } else {
                            for (Player player : portablePlayer) {
                                chatMessageFormater.sendTo(player, "For porting into another world you need DIAMOND block as core.");
                            }
                            return;
                        }
                    }

                    if (portablePlayer.size() > 0) {
                        Location targetLocation = new Location(targetWorld, targetX + 0.5, targetY + 0.5, targetZ + 0.5);

                        if (platformChecker.chechPlatform(targetLocation, true)) {
                            for (Player player : portablePlayer) {
                                if ((flagsByte & 0b00000001) != 0) {
                                    if (!player.getInventory().isEmpty()) {
                                        chatMessageFormater.sendTo(player, "This Homeport target requires empty inventory.");
                                        continue;
                                    }
                                }
                                world.spawnParticle(Particle.ELECTRIC_SPARK, player.getLocation(), 5000, 0.5, 2, 0.5);
                                player.teleport(targetLocation);
                                world.spawnParticle(Particle.ELECTRIC_SPARK, player.getLocation(), 5000, 0.5, 2, 0.5);
                                // player.getInventory().isEmpty();
                                chatMessageFormater.sendTo(player, "You have been HomePorted! Have a nice day.");
                            }
                        } else {
                            for (Player player : portablePlayer) {
                                chatMessageFormater.sendTo(player, "Target platform not ready.");
                            }
                        }
                    }
                    ////////////////////////////////////////////////////////////////////////////////////
                }
            }
        }
    }
}
