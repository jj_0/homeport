package nick.jj.homeport.base;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

public class platformChecker {
    public static boolean chechPlatform(Location location, Boolean isTarget) {
        World world = location.getWorld();
        int centerX = location.getBlockX();
        int centerY = location.getBlockY();
        int centerZ = location.getBlockZ();
        for (int x = centerX - 1; x < centerX+2; x++) {
            for (int z = centerZ - 1; z < centerZ+2; z++) {
                Material material = world.getBlockAt(x, centerY - 1, z).getType();
                // Bukkit.broadcastMessage(x + ":" + centerY + ":" + z + " >> " + world.getBlockAt(x, centerY - 1, z).getType().toString());
                if ((x == centerX) && (z == centerZ)) {
                    if (isTarget) {
                        if (material != Material.AMETHYST_BLOCK) {return false;};
                    } else {
                        if (material != Material.GOLD_BLOCK) {return false;};
                    }
                } else {
                    if (material != Material.IRON_BLOCK) {return false;};
                }
            }
        }
        return true;
    }
}
