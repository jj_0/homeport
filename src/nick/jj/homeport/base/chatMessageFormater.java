package nick.jj.homeport.base;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class chatMessageFormater {
    public static void sendTo(CommandSender sender, String text) {
        sender.sendMessage(format(text));
    }

    public static void sendTo(Player player, String text) {
        player.sendMessage(format(text));
    }
    public static String format(String text) {
        return (ChatColor.GOLD + "[HomePort] ") + (ChatColor.WHITE + text);
    }
}
