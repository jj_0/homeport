package nick.jj.homeport.base;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;


public class playerGetter {
    public static ArrayList<Player> nearLocation(Location location, Integer distanceHorizontal, Integer destanceVertical) {
        ArrayList<Player> playerList = new ArrayList<Player>();
        for (Player player : location.getWorld().getPlayers()) {
            Location  playerLocation = player.getLocation();
            if (
                    (Math.abs(playerLocation.getBlockX() - location.getBlockX()) < distanceHorizontal) &&
                            (Math.abs(playerLocation.getBlockY() - location.getBlockY()) < destanceVertical) &&
                            (Math.abs(playerLocation.getBlockZ() - location.getBlockZ()) < distanceHorizontal)
            ) {playerList.add(player);}
        }
        return  playerList;
    }
}
