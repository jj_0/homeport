package nick.jj.homeport;

import nick.jj.homeport.base.chatMessageFormater;
import nick.jj.homeport.commands.homePortCommands;
import nick.jj.homeport.events.homePortEvents;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.metadata.Metadatable;
import org.bukkit.plugin.java.JavaPlugin;

public class homePort extends JavaPlugin {

    @Override
    public void onEnable() {
        Server server = getServer();
        server.getPluginManager().registerEvents(new homePortEvents(this), this);
        getCommand("homeport").setExecutor(new homePortCommands(this));
        server.getConsoleSender().sendMessage(chatMessageFormater.format("Loaded"));

    }

    @Override
    public void onDisable() {

    }
}
