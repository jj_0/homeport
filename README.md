# HomePort

Homeport is really simple way how to enable fast travel on your server. This plugin can port not only into nether but even between worlds. For teleporting between worlds you need to use diamond block core. 

For Minecraft 1.17

[![Video](https://img.youtube.com/vi/89wmtV8bDHU/0.jpg)](https://www.youtube.com/watch?v=89wmtV8bDHU)



## How to build:
### Target platform
To create target platform place one *amethyst block* and 8 *blocks of iron* around.
![target platform](images/targetPlatform.png)

### Teleporter platform
Start with *gold block* with 8 *blocks of iron* around.
![teleporter platform base](images/teleportPlatformBase.png)
On top of gold block place another *gold block* or *diamond block*, on tom of that place *glowing item frame* and on sides place *stone button*.  
![teleporter platform full](images/teleportPlatformFull.png)

## Usage
### Creating target book
Target book stores information of target platform. To create one stand on the gold block of target platform with book in hand and use command `/homeport settarget`. This creates book containing information about position of this target platform.

### Teleporting
On teleporter platform place target book into the frame, stand on the platform and press button. Congratulation, you have been teleported.

